from game.bots.script import ScriptBot
import sys
import socket
from scipy.optimize import curve_fit as fit
import numpy as np

def parabel(x,a, b, c):
    return a*x**2 + b*x + c
    
def linear(x,p1,p2):
  x1,y1 = p1
  x2,y2 = p2
  m = (y2-y1)/(x2-x1)
  t = y1-m*x1
  return m*x+t
  
def fit_curvatures(bot):
  
  for points_name in bot.track.curvatures:
    points = bot.track.curvatures[points_name]
    if not not points:
      min_point = min(points,key = lambda item: item[1])
      max_point = max(points,key = lambda item: item[1])
      x_middle = (min_point[0]+max_point[0])/2.
      sorted_points = sorted([(abs(x[0]-x_middle),x[1]) for x in points],key= lambda item: item[0])
      for point in sorted_points:
        if point[1] <= linear(x_middle,min_point,max_point):
          mid_point = point
          break
      x1,y1 = min_point
      x2,y2 = max_point
      x3,y3 = mid_point
      a = (x3*(y2-y1)+x2*(y1-y3)+x1*(y3-y2))/((x1-x2)*(x1-x3)*(x2-x3))
      b = (x3**2 * (y1-y2) + x1**2 * (y2 - y3) + x2**2 * (y3 - y1))/((x1-x2)*(x1-x3)*(x2-x3))
      c = (x3*(x2*(x2-x3)*y1 + x1 * (x3-x1) * y2) + x1 * (x1-x2) * x2 * y3)/((x1-x2)*(x1-x3)*(x2-x3))
      x_ = [x[0] for x in points]
      y_ = [y[1] for y in points]
      (a,b,c),cov = fit(parabel, np.array(x_), np.array(y_), [a, b, c])
      bot.track.curvatures_param[points_name] = (a,b,c)
      #TODO: (a,b,c) verwerten
  
    

def script(bot):
    
    while bot.myCar.position['piecePosition']['lap'] is 0:
      for car in bot.cars.values():
        piece_index  = car.position['piecePosition']['pieceIndex']
        lane_index = car.position['piecePosition']['lane']['startLaneIndex']
        piece = bot.track.pieces[piece_index]
        if not piece.switch and piece.radius != 0:
          curvature = piece.curvatureAt(lane_index,lane_index,0)
          if abs(car.angleAcc) >=.1:
            bot.track.curvatures[curvature].append((car.speed,car.angleAcc))
      yield 0.5
    
    fit_curvatures(bot)
    #TODO
      
    while True:
      yield 0.4

bot = ScriptBot()
bot.script = script(bot)


def onFinish():
  print "We are done."