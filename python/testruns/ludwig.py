from game.bots.script import ScriptBot
import sys
import socket

def script(bot):
    
    while True:
      #print bot.myCar.position#['piecePosition']['pieceIndex']
      if bot.myCar.position['piecePosition']['pieceIndex']+1 >= len(bot.myCar.track.pieces):
        next_piece = bot.myCar.track.pieces[0]
      else:
        next_piece = bot.myCar.track.pieces[bot.myCar.position['piecePosition']['pieceIndex']+1]
      if next_piece.switch:
        way = bot.track.graph.best_way
        #print "way",way
        bot.track.graph.make_step(way)
        while bot.myCar.track.pieces[bot.myCar.position['piecePosition']['pieceIndex']+1].switch:
          #print "best way", way
          #print "ac_lane", bot.myCar.position['piecePosition']['lane']['startLaneIndex']
          if way is "Straight":
            #print "1.6"
            yield 0.4
          else:
            #print way
            yield way
            
      else:
        #print "0.6"
        yield 0.4

bot = ScriptBot()
bot.script = script(bot)


def onFinish():
  print "We are done."
  