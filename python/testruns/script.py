from game.bots.script import ScriptBot
import sys
import socket

def script(bot):
    while bot.myCar.position['piecePosition']['lap'] == 0:
        yield 0.6
    yield "Right"
    while True:
        yield 0.6

bot = ScriptBot()
bot.script = script(bot)


def onFinish():
  print "We are done."
  