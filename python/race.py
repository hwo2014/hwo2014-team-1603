import argparse
import sys
import config
import socket
import time
import random
import string

from threading import Thread


class BotThread(Thread):
  def __init__(self, bot):
    Thread.__init__(self) 
    self.bot = bot
    self.daemon = True
  
  def run(self):
    self.bot.run()

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  
  parser.add_argument("module")
  parser.add_argument("--name", default="PallasBot", action='append')
  parser.add_argument("--bot", default="bot")
  parser.add_argument("--host", default=config.host)
  parser.add_argument("--port", default=config.port, type=int)
  parser.add_argument("--key", default=config.key)
  parser.add_argument("-t","--trackName", default=config.track)
  
  args = parser.parse_args()
  
  mod = __import__(args.module, globals(), locals(),[args.bot])
  bot = getattr(mod, args.bot)
  
  race_password = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(15))
  
  onFinish = lambda: None
  if hasattr(mod, 'onFinish'):
    onFinish = getattr(mod, 'onFinish')
  
  host = args.host
  port = args.port
  key = args.key
  name = args.name
  
  
  if type(bot) is not list: 
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(host, port, name, key))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))

    bot.key = key
    bot.name = name
    bot.carCount = 1
    bot.password = race_password
    bot.socket = s
    bot.trackName = args.trackName
    
    bot.joinRace()
    try:
      bot.run()
    except (KeyboardInterrupt, SystemExit):
      print '\n! Received keyboard interrupt, bye.\n'
  else:
    
    bot_threads = []
    
    bot_num = 0
    for b in bot:
      s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      s.connect((host, int(port)))  
      b.password = race_password
      b.key = key
      b.name = name + "-" + str(bot_num)
      b.socket = s
      b.carCount = len(bot)
      b.trackName = args.trackName
      bot_num += 1
      
    
    first_bot = bot[0]
    first_bot.createRace()
    
    first_bot_thread = BotThread(first_bot)
    first_bot_thread.start()
    
    bot_threads.append(first_bot_thread)
    
    time.sleep(1)
    
    for b in bot[1:]:
      b.joinRace()
      bot_thread = BotThread(b)      
      bot_thread.start()
      bot_threads.append(bot_thread)
    
    try:
      while len([x for x in bot_threads if x.is_alive()]) > 0:
        time.sleep(1)
    except (KeyboardInterrupt, SystemExit):
      print '\n! Received keyboard interrupt, quitting threads.\n'
  onFinish()
    
  