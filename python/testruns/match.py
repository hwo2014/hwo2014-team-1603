from game.bots.script import ScriptBot

def script(bot):
    while bot.myCar.position['piecePosition']['lap'] == 0:
        yield 0.6
    yield "Right"
    while True:
        yield 0.6

def script2(bot):
    while bot.myCar.position['piecePosition']['lap'] == 0:
        yield 0.6
    yield "Right"
    while True:
        yield 0.6

bot1 = ScriptBot()
bot1.script = script(bot1)

bot2 = ScriptBot()
bot2.script = script2(bot2)

bot = [bot1, bot2]