from game.bots.script import ScriptBot
import sys
import socket

def script(bot):
    throttle = 1.0
    car = bot.myCar
    for i in xrange(5):
        yield 1.0
    print "P ", car.update_power(1.0, 0.5)
    yield 1.0
    print "beta ", car.update_beta(1.0, 0.5)
    yield throttle
    
    lap = car.position['piecePosition']['lap']
    while True:
        if lap < car.position['piecePosition']['lap']:
          #yield "Right"
          throttle -= 0.1
          lap = car.position['piecePosition']['lap']
        yield throttle
        if abs(car.F_ges(throttle) - car.mass*car.acceleration) > 1.0e-8:
            if car.speed > 1.0e-8:
                print car.F_ges(throttle) - car.mass*car.acceleration, " !!! "
        if abs(car.M_ges(throttle) - car.moment*car.angleAcc) > 1.0e-8:  
            pieceIndex = car.position['piecePosition']['pieceIndex']
            curve = car.track.pieces[pieceIndex].curvatureAt(car.position['piecePosition']['lane']['startLaneIndex'], car.position['piecePosition']['lane']['endLaneIndex'], car.position['piecePosition']['inPieceDistance'])
            print car.M_zentri(throttle), car.M_ges(throttle), car.moment*car.angleAcc, car.angleAcc, car.speed, car.angle, curve
    

bot = ScriptBot()
bot.script = script(bot)


def onFinish():
  print "We are done."
  #print "v = ", bot.cars['red'].speeds[0:200]
  #print "a = ", bot.cars['red'].accelerations[0:200]
  #print "phi = ", bot.cars['red'].angles[0:200]
  #print "track = ", bot.track
  
