//
//  track.h
//  plusbot
//
//  Created by Matthias on 16.04.14.
//
//

#ifndef __plusbot__track__
#define __plusbot__track__

#include <iostream>
#include <vector>

class Piece {
  bool swtch;
  float length;
  float radius;
  float angle;
};

class Track {
public:
  std::vector<Piece> pieces;
};

#endif /* defined(__plusbot__track__) */
