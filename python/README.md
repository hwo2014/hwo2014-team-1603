# Bot test runnner:

- create a new test run in testruns/yourtestrun.py

- Post-race analysis: 	if present, your testruns onFinish() function will be called once the race is completed. Example: testruns/script.py

- run: python race.py testruns.yourtestrun

# race.py

Run 

    python race.py

and you'll see all available options
    usage: race.py [-h] [--name NAME] [--bot BOT] [--host HOST] [--port PORT]
                   [--key KEY] [-t TRACKNAME]
                   module

## Python bot template for HWO-2014

Install (OSX):

    brew install python
    pip install virtualenv

Install (Debian/Ubuntu):

    sudo apt-get install python-pip
    sudo pip install virtualenv
