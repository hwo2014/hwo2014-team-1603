# -*- coding: utf-8 -*-

class Tree(object):
  __slots__ = ['node_zero','ac_node']

  
  def __init__(self,items,lanes):
    
    num_lanes = 0
    start_pos = [0,0]
    
    for lane in lanes:
      if lane == None:
        break
      num_lanes +=1
    
    self.node_zero = Node(start_pos,None,items,lanes,num_lanes,None,self)
    self.ac_node = self.node_zero
  
  def set_start(self,lane_index):
    self.ac_node = self.node_zero.children[lane_index]
    
  def step_node(self,direction):
    pos = self.ac_node.position[0]
    #print "ac_pos",self.ac_node.position
    num_lanes = self.ac_node.num_lanes
    lane_ind = self.ac_node.lane_ind
    if direction is "Left":
      lane_index = lane_ind - 1
    elif direction is "Right":
      lane_index = lane_ind + 1
    else:
      lane_index = lane_ind
      
    if type(self.ac_node.children[0]) is Tree:
      self.ac_node = self.node_zero.children[lane_index]
    else:
      self.ac_node = self.ac_node.children[lane_index]
    #print "new_pos",self.ac_node.position
    
  def get_next_lane(self):
    
    return self.ac_node.get_min_way()
   
      
    
class Node(object):
  __slots__ = ['item','parent','children','weight','position','num_lanes','lane_ind']  
  
  def make_children(self,items,lanes,tree):
      children = []
      if self.position[0]+1 >= len(items):
        return [tree]
      elif self.position[0] is 0:
        for n in xrange(self.num_lanes):
          new_pos = [1,n]
          children.append(Node(new_pos,self,items,lanes,self.num_lanes,n,tree))
        return children
        
      #print "startlane",self.parent.lane_ind
      #print "endlane",self.lane_ind
    
      for n in xrange(self.num_lanes):
        if abs(self.lane_ind-n) <= 1:
          new_pos = [self.position[0]+1,self.position[1]*self.num_lanes+n]
          children.append(Node(new_pos,self,items,lanes,self.num_lanes,n,tree))
        else:
          children.append(None)
      return children
  
    
  def __init__(self,position,parent,items,lanes,num_lanes,lane_ind,tree):
    self.position = position 
    if position[0] is 0:
      self.item = None
    else:
      self.item = items[position[0]-1]
    self.num_lanes = num_lanes
    self.lane_ind = lane_ind
    self.parent = parent
    
    self.children = self.make_children(items,lanes,tree)
    
    if type(self.children[0]) is Tree:
      self.weight = self.calc_weight(lanes)
    elif self.item == None:
      self.weight = None
    else:
      self.weight = self.calc_weight(lanes) + min(filter(lambda x: x is not None,self.children),key = lambda item: item.weight).weight

  
  def calc_weight(self,lanes):
    #TODO
    weight = 0
    #print "parent",self.parent
    #print "position",self.position


    for piece in self.item:
      #weight += piece.angle
      weight += piece.lengthAt(self.lane_ind,self.lane_ind)
      
    #print "startlane",startlane
    #print "endlane",endlane
    return weight

  def get_min_way(self):
    index = 0
    if type(self.children[0]) is Tree:
      
      old_val = None
      
      for i in xrange(len(self.children[0].node_zero.children)):
        if self.children[0].node_zero.children[i] is not None:
          if old_val is None:
            if self.children[0].node_zero.children[i].weight is not None:
              old_val = self.children[0].node_zero.children[i].weight
              index = i
          elif self.children[0].node_zero.children[i].weight <= old_val:
            old_val = self.children[0].node_zero.children[i].weight
            index = i
    else:
      old_val = None
      for i in xrange(len(self.children)):
        if self.children[i] is not None:
          if old_val is None:
            if self.children[0].weight is not None:
              old_val = self.children[0].weight
              index = i
          elif self.children[i].weight <= old_val:
            old_val = self.children[i].weight
            index = i
    '''
    tmp = []
    if type(self.children[0]) is Tree:
      tmp_list = self.children[0].node_zero.children
    else:
      tmp_list = self.children
    for child in tmp_list:
      tmp.append(child.weight)
    print "------"
    print "children",tmp_list
    print "weights",tmp
    print "startlane",self.lane_ind
    print "endlane", index
    print "diff",self.lane_ind-index
    '''
    
    if self.lane_ind-index is -1:
      return "Right"
    elif self.lane_ind-index is 1:
      return "Left"
    else:
      return "Straight"
  


    
    