# -*- coding: utf-8 -*-

from __future__ import division
from math import pi
from math import sqrt
from math import sin
from math import cos
from math import floor
from math import exp
from .tree import Tree

class Piece(object):
  __slots__ = ['length', 'radius', 'switch', 'angle', 'lanes']
  
  def __init__(self, length, radius, switch, angle):
    
    if length is None:
      length = abs(radius*angle*pi/180.0)
    self.length = length
    self.radius = radius
    self.switch = switch
    self.angle = angle
    
  def string(self):
      return "l, r, s, a, lanes = " + str(self.length)+ " , " + str(self.radius) + " , " + str(self.switch)+ " , " + str(self.angle)+ " , " + str(self.lanes)
  
  def lengthAt(self, startLane, endLane): #länge der strecke auf dem gesamten piece
    #q = 100.0*pi/4.0
    f1 = 1.00078333414  # faktor für gerade angepasst!!!
    # 0.99977493743 für 45 grad 100 radius
    # 1.00010305123 für 22.5 grad 200 radius
    f2 = 1.00043116503-abs(self.angle)*1.4582835556e-5  # faktor mit kurven angepasst :D
    
    if self.angle == 0:
        if startLane == endLane:
            return self.length
        else:
            return f1*sqrt(self.length**2 + (self.lanes[startLane] -self.lanes[endLane])**2)
    else:
        if startLane == endLane:
            if self.angle > 0.:
                r = self.radius - self.lanes[startLane]
                return r*self.angle*pi/180.0 
            else:
                r = self.radius + self.lanes[startLane]
                return -r*self.angle*pi/180.0 
        else:
            if self.angle > 0.:
                r = self.radius - (self.lanes[startLane] +self.lanes[endLane])/2.0
            else:
                r = self.radius + (self.lanes[startLane] +self.lanes[endLane])/2.0
            return f2*sqrt((r*self.angle*pi/180.0)**2 +  (self.lanes[startLane] -self.lanes[endLane])**2)
    
  def curvatureAt(self, startLane, endLane, inPiecePos):
      if self.angle == 0:
          if startLane == endLane:
              return 0.0
          else:
              return 0.0 # TODO verbessern
      else:
          if startLane == endLane:
              if self.angle > 0.:
                  r = self.radius - self.lanes[startLane]
                  return 1.0/r
              else:
                  r = self.radius + self.lanes[startLane]
                  return -1.0/r
          else:
              if self.angle > 0.:
                  return 1.0/self.radius #TODO verbessern
              else:
                  return -1.0/r

class Track_Graph(object):
  
  __slots__ = ['track','switches','graph']
  
  
  def __init__(self,track):
    #Reihenfolge in track.pieces??
    tmp = []
    self.switches = []

    for n in xrange(len(track.pieces)):
        piece = track.pieces[n]
        tmp.append(piece)
        if piece.switch:
            self.switches.append(tmp)
            tmp = []
    if not tmp:
      new = tmp.append(self.switches[0])
      self.switches[0] = new 
      
    self.switches.append(self.switches[0])
    
    self.graph = Tree(self.switches,track.lanes)
  
  def make_step(self,way):
    self.graph.step_node(way)
    
  def set_start_point(self,lane_index):
    self.graph.set_start(lane_index)
    
  @property
  def best_way(self):
    return self.graph.get_next_lane()

class Track(object):

  __slots__ = ['pieces', 'lanes','graph','curvatures','curvatures_param']
  
  def __init__(self,pieces,lanes):
    
    self.pieces = pieces
    self.lanes = lanes
    self.graph = Track_Graph(self)
    self.curvatures = {}
    
    self.calc_curvatures()
    
  def calc_curvatures(self):
    for piece in self.pieces:
      if not piece.switch and piece.radius != 0:
        for i in xrange(len(self.lanes)):
          if self.lanes[i] is None:
            break
          curvature = piece.curvatureAt(i,i,0)
          if curvature not in self.curvatures:
            self.curvatures[curvature]=[]
            
    


class Car(object):
  #__slots__ = ['guideFlag', 'width', 'length', 'last_position', 'update_position', 'track', 'angle', 'speed']
  positions = []
  speeds = []
  accelerations = []
  angles = []
  angleVelos = []
  angleAccs = []
  
  length = None
  width = None
  guideFlag = None
  mass = None
  moment = None
  massDensity = 1.0
  #power = 163.265306123          # F_acc = power*throttle
  power = 8000.0 / 49.0
  #alpha = 0.0     #Reibungskoeffizierten
  #beta = 16.326530612304109      # F_br = alpha*v^2 + beta*v + gamma
  beta = 800.0 / 49.0
  #gamma = 0.0    gamma fliegt raus, auf keimola und germany getestet 
  #               alpha ist auch null, keimola!
  cA = dict()
  cB = dict()
  cv0 = dict()
  #backDriftA = 1.0  # Koeffizient für das rückstellende Drehmoment M_back = backdrift*angle
  #backDriftB = 1.0
  backDrift = 23703.703703703704 # Neuer gesamtparameter mit M_back = -backQ*speed*angle - (backDrift-backQ*speed)*angleVelo
  # = 6 400 000 / 27
  backQ = 296.29629629629630
  # = 8 000 / 27
  # Neu: backDriftA,B hängen von speed ab!! Lineare funktionen!!  
  # backDriftA = const*speed
  # backDriftB = const1 - const2*speed, backDriftA + backDriftB = const
  
  
  # moment * angleAcc = M_back = -backDriftA * angle -backDriftB * angleVelo
  
  #    / phi1  phiD1 \  |backDriftA |             |phiDD1|
  #    |             |  |           |  =  moment* |      | + friction*...
  #    \ phi2  phiD2 /  |backDriftB |             |phiDD2|
  
  
  def __init__(self, length, width, guideFlag):
    self.last_position = None
    self.length = length
    self.width = width
    self.guideFlag = guideFlag
    self.mass = self.massDensity * self.length * self.width
    self.moment = self.mass*((self.length**2 + self.width**2)/12.0 + self.guideFlag**2)
    
    
    
    
  def F_ges(self, throttle):
      F_br = self.speed*self.beta
      F_acc = self.power*throttle
      return F_acc - F_br
      
      
  #def F_zentri(self):
  #    pieceIndex = self.position['piecePosition']['pieceIndex']
  #    curve = self.track.pieces[pieceIndex].curvatureAt(self.position['piecePosition']['lane']['startLaneIndex'], self.position['piecePosition']['lane']['endLaneIndex'], self.position['piecePosition']['inPieceDistance'])
  #    return self.mass*self.speed**2 * curve
      
      
  def M_ges(self, throttle):
      M_accbr = 0.0#self.F_ges(throttle)*self.guideFlag*sin(self.angle*180.0/pi)*self.friction
      M_zentri = self.F_zentri() * self.guideFlag*cos(self.angle*180.0/pi)*self.friction
      M_back = -self.backQ*self.speed*self.angle - (self.backDrift-self.backQ*self.speed)*self.angleVelo
      return M_zentri + M_back

  def M_zentri(self, throttle):
      return self.F_zentri() * self.guideFlag*cos(self.angle*180.0/pi)*self.friction
      
      
  def update_power(self, power, weight = 1.0):
      old = self.power
      new = power
      self.power = weight*new + (1.0-weight)*old
      return self.power
      
  def update_beta(self, beta, weight = 1.0):
      old = self.beta
      new = beta
      self.beta = weight*new + (1.0 - weight)*old
      return self.beta
      
  def update_backDrift(self, throttle, throttle_old, weight = 1.0):
      if abs(self.angle) < 1.0e-12:
          return
      if self.speed == 0.0:
          return
      if len(self.angles) <= 1:
          return
      phi1 = self.angles[-1]
      phi2 = self.angles[-2]
      phiD1 = self.angleVelos[-1]
      phiD2 = self.angleVelos[-2]
      phiDD1 = self.angleAccs[-1]
      phiDD2 = self.angleAccs[-2]
      R1 = self.guideFlag*self.friction*(self.F_ges(throttle)*sin(phi1*180.0/pi)+self.F_zentri()*cos(phi1*180.0/pi))-self.moment*phiDD1
      R2 = self.guideFlag*self.friction*(self.F_ges(throttle)*sin(phi2*180.0/pi)+self.F_zentri()*cos(phi2*180.0/pi))-self.moment*phiDD2
      det = phi1*phiD2 - phi2*phiD1
      if abs(det) < 1.0e-12:
          return
      old1 = self.backDrift
      old2 = self.backQ
      bDA = (phiD2*R1 - phiD1*R2)/det
      bDB = (phi1*R2 - phi2*R1)/det
      new1 = bDA+bDB
      new2 = bDA/self.speed
      self.backDrift = weight*new1 + (1.0 - weight)*old1
      self.backQ = weight*new2 + (1.0 - weight)*old2
      return self.backDrift, self.backQ
      
  def update_friction(self, throttle, weight = 1.0):
      q = (self.F_ges(throttle)*sin(self.angle*180.0/pi) + self.F_zentri()*cos(self.angle*180.0/pi))*self.guideFlag
      if abs(q) < 1.0e-12:
          return
      old = self.friction
      M_back = -self.backQ*self.speed*self.angle - (self.backDrift-self.backQ*self.speed)*self.angleVelo
      new = (self.moment*self.angleAcc - M_back)/q
      self.friction = weight*new + (1.0 - weight)*old
      if self.friction < 0.0:
          self.friction = 0.0
      #if self.friction > 1.0:
      #    self.friction = 1.0
      return self.friction

  
  def update_position(self, pos):
    self.positions.append(pos)
    self.angles.append(pos['angle'])
    
    # Assumption: max 1 piece per distance
    if len(self.positions) > 1:
      last_position = self.positions[-2]
      
      if last_position['piecePosition']['pieceIndex'] == pos['piecePosition']['pieceIndex']:
        self.speeds.append(pos['piecePosition']['inPieceDistance']-
          last_position['piecePosition']['inPieceDistance'])
      else:
        startLane = last_position['piecePosition']['lane']['startLaneIndex']
        endLane = last_position['piecePosition']['lane']['endLaneIndex']        
        
        last_piece = self.track.pieces[last_position['piecePosition']['pieceIndex']]
        last_piece_distance = last_piece.lengthAt(startLane,endLane) - last_position['piecePosition']['inPieceDistance']
        self.speeds.append(pos['piecePosition']['inPieceDistance'] + last_piece_distance)
        
    else:
      self.speeds.append(0.)
      
    if len(self.speeds) > 1:
        self.accelerations.append(self.speeds[-1] - self.speeds[-2])
    else:
        self.accelerations.append(0.)
    
    if len(self.angles) > 1:
        self.angleVelos.append(self.angles[-1] - self.angles[-2])
    else:
        self.angleVelos.append(0.)
        
    if len(self.angleVelos) > 1:
        self.angleAccs.append(self.angleVelos[-1] - self.angleVelos[-2])
    else:
        self.angleAccs.append(0.)

  def drive_fast(self, distance, endSpeed):
      t_stop = (self.mass*(endSpeed-self.speed) + self.beta*distance)/self.power
      if t_stop <= 0.0:
          return (0.0, 1), (0.0, 0), (0.0, 0)
      t_k = float(int(t_stop))
      t_r = t_k - t_stop # TODO
      if t_k < 1.0:
          return (t_r, 1), (0.0, 1), (0.0, 0)
      return (1.0, t_k), (t_r, 1), (0.0, 1)

  def drive_slow(self, maxSpeed):
      return
      
  def interpolate_zentriParams():
        return

  @property
  def angle(self):
      return self.angles[-1]
      
  @property
  def angleVelo(self):
      return self.angleVelos[-1]
      
  @property
  def angleAcc(self):
      return self.angleAccs[-1]
    
  @property
  def speed(self):
      return self.speeds[-1]
      
  @property
  def acceleration(self):
      return self.accelerations[-1]
      
  @property
  def position(self):
      return self.positions[-1]
      
      
      
      
class Physics(object):
    
    def calc_beta(self, car, index = -1, throttle = 0.0):
        index = index % len(car.speeds)
        speed = car.speeds[index]
        acc = car.accelerations[index]
        if speed <= 0.0:
            return
        return (car.power*throttle - car.mass*acc)/speed
        
    def calc_power(self, car, index = -1, throttle = 1.0):
        if throttle <= 0.0:
            return
        index = index % len(car.speeds)
        speed = car.speeds[index]
        acc = car.accelerations[index]
        return (car.mass*acc + car.beta*speed)/throttle

    def calc_backParams():
        return
        
    def calc_zentriParams():
        return
        
    def interpolate_zentriParams():
        return
        
