//
//  car.h
//  plusbot
//
//  Created by Matthias on 16.04.14.
//
//

#ifndef __plusbot__car__
#define __plusbot__car__

#include <iostream>
#include "track.h"

class Car {
public:
  float speed();
  float accelleration();
  void addPosition(float position, int pieceIndex);
  Track *track;
  
};


#endif /* defined(__plusbot__car__) */
