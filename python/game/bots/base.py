import json
from ..gameobjects import Car, Track, Piece

class Bot(object):
    cars = {}
    def __init__(self, socket=None, name=None, key=None):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")
        
    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
                                 
    def joinRace(self):
        return self.msg("joinRace", {
          "botId": {
            "name": self.name,
            "key": self.key
            },
         "trackName": self.trackName,
         "password": self.password,
         "carCount": self.carCount
         })
                                
    def createRace(self):
      return self.msg("createRace", {
        "botId": {
          "name": self.name,
          "key": self.key
          },
       "trackName": self.trackName,
       "password": self.password,
       "carCount": self.carCount
       })
      
    def throttle(self, throttle):
        self.msg("throttle", throttle)
        
    def switchLane(self, side):
        self.msg("switchLane", side)
        
    def ping(self):
        self.msg("ping", {})

    def run(self):
        #self.join()
        self.msg_loop()
    
    def on_init(self, data):
      
      print "Game init"
      
      race = data['race']
      
      lanes = [None,None,None,None]
      
      for l in race['track']['lanes']:
        lanes[l['index']] = l['distanceFromCenter']
      
      
      pieces = []
      for l in race['track']['pieces']:
        radius = 0.
        switch = False
        angle = 0.
        length = None
        
        if 'radius' in l:
          radius = l['radius']
        if 'switch' in l:
          switch = l['switch']
        if 'angle' in l:
          angle = l['angle']
        if 'length' in l:
          length = l['length']
          
        piece = Piece(length, radius, switch, angle)
        piece.lanes = lanes
        pieces.append(piece)
      
      track = Track(pieces,lanes)
      self.track = track
      self.track.graph.set_start_point(0)
      
      for c in race["cars"]:
        car = Car(c['dimensions']['length'], c['dimensions']['width'], c['dimensions']['guideFlagPosition'])
        #car.width = c['dimensions']['width']  Veraltet, da in __init__ enthalten
        #car.length = c['dimensions']['length']
        #car.guideFlagPosition = c['dimensions']['guideFlagPosition']
        car.track = track
        self.cars[c['id']['color']] = car
    
      self.myCar = self.cars[self.myColor]
      self.ping()
      
    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        for pos in data:
          color = pos['id']['color']
          car = self.cars[color]
          car.update_position(pos)
          
        self.drive()

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()
        
    def on_your_car(self, data):
        self.myColor = data['color']

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            #'joinRace': self.on_join_race,
            #'createRace': self.on_create_race,
            'gameInit': self.on_init,
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'yourCar': self.on_your_car,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            #print msg
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg))
                self.ping()
            line = socket_file.readline()
