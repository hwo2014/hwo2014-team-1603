# -*- coding: utf-8 -*-
"""
Created on Thu Apr 17 18:15:40 2014

@author: Tobias
"""

from math import cos
import numpy as np
import pylab as plt
from scipy.optimize import curve_fit as fit


#d = np.loadtxt("data3.txt", skiprows=0)
#
#[t,F,phi,phiD,phiDD,v,a,Fz] = np.transpose(d)
#phiDot = [phi[i]-phi[i-1] for i in range(len(phi))]

d = np.loadtxt("data8.txt", skiprows=1)

[Mz, Mg, M, phiDD, v, phi, c] = np.transpose(d)

def parabel(x,a,x0,c):
    return a*(x-x0)**2+c

Y = M-(Mg-Mz)

def inter(i):
    return True
    if +c[i] < 0.015 and +c[i] > 0.01:
        if abs(Y[i]) > 1000.0:
#            if v[i] > 5.0:
                return True
    return False



MX = [M[i] for i in xrange(len(M)) if inter(i)]
vX = [v[i] for i in xrange(len(M)) if inter(i)]
phiX = [phi[i] for i in xrange(len(M)) if inter(i)]
cX = [c[i] for i in xrange(len(M)) if inter(i)]
YX = [Y[i] for i in xrange(len(M)) if inter(i)]

print cX[0]



plt.plot(vX,YX,".")
#plt.plot(vX,[cos(i) for i in phi],".")

(a,x0,c),cov = fit(parabel, np.array(vX), np.array(YX), [-10000.0, 1.0, 100000.0])

#(m,b),cov = fit(linear, np.array(vX), np.array(MrX), [1000.0, -1000.0])

#plt.plot(np.arange(0.0,12.0,0.1), [parabel(i,13817.0, 2.81498, -95139) for i in np.arange(0.0,12.0,0.1)])
#plt.plot(np.arange(0.0,12.0,0.1), [parabel(i,12407.0, 3.05729, -107153) for i in np.arange(0.0,12.0,0.1)])
#plt.plot(np.arange(0.0,12.0,0.1), [parabel(i,9596.2, 4.04084, -137161) for i in np.arange(0.0,12.0,0.1)])
#plt.plot(np.arange(0.0,12.0,0.1), [parabel(i,8595.2, 4.17083, -153955) for i in np.arange(0.0,12.0,0.1)])

def curve(x, a, b):
    return a*(abs(x)**b)
#xs = [90, 110, 190, 210]
#ys = [5.4391, 5.9961, 7.8314, 8.4030]

#(a,b), cov = fit(curve, np.array(xs), np.array(ys))

#print a,b
#print cov

#plt.plot(xs,ys)


print a, x0, c
#print m, b

#print cov


plt.plot(np.arange(2.0,7.0,0.01), [parabel(i,a,x0,c) for i in np.arange(2.0,7.0,0.01)])

##[t,bDA,bDB,phi,phiD,phiDD,v,a,Fz] = np.transpose(d)
##phiDot = [phi[i]-phi[i-1] for i in range(len(phi))]
##
##
##plt.plot(v,bDB,".")
##for i in range(len(v)):
##    print bDA[i]+bDB[i]
#plt.plot(v,bDB)
#plt.plot(phi,F,".")
#plt.xscale("log")
#plt.plot(a[39:])
#for i in range(len(phi)):
#    print phi[i], back[i], abs(phi[i]) * abs(back[i])
plt.show()
