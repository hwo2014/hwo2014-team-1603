# -*- coding: utf-8 -*-
"""
Created on Thu Apr 17 18:15:40 2014

@author: Tobias
"""

from math import cos
import numpy as np
import pylab as plt
from scipy.optimize import curve_fit as fit


#d = np.loadtxt("data3.txt", skiprows=0)
#
#[t,F,phi,phiD,phiDD,v,a,Fz] = np.transpose(d)
#phiDot = [phi[i]-phi[i-1] for i in range(len(phi))]

d = np.loadtxt("data9.txt")

[Mz, Mg, M, phiDD, v, phi, c] = np.transpose(d)

# def parabel(x,a,x0,c):
#     return a*(x-x0)**2+c


vm = 10.
def parabel(v,v2, A, B):
    return -(((v - v2)*(A*(2*v + v2 - vm)*(3*v2 - vm) + 4*B*(v - vm)*(-v2 + vm)))/((v2 - vm)*(3*v2 - vm)*(v2 + vm)))


Y = M-(Mg-Mz)

def inter(i):
    if +c[i] < 0.0047 and +c[i] > .004:
        if abs(Y[i]) > 1000.0:
          return True
    return False
    
def inter_2(i):
    if +c[i] < 0.0052 and +c[i] > .0049:
        if abs(Y[i]) > 1000.0:
          return True
    return False

def inter_3(i):
    if +c[i] < 0.0060 and +c[i] > .0053:
        if abs(Y[i]) > 1000.0:
          return True
    return False

plt_rnge= np.arange(-0.,11.0,0.01)
def fit_it():  
  MX = [M[i] for i in xrange(len(M)) if flt(i)]
  vX = [v[i] for i in xrange(len(M)) if flt(i)]
  phiX = [phi[i] for i in xrange(len(M)) if flt(i)]
  cX = [c[i] for i in xrange(len(M)) if flt(i)]
  YX = [Y[i] for i in xrange(len(M)) if flt(i)]
  print cX[0]

  plt.plot(vX,YX,".")
  (a,x0,c_),cov = fit(parabel, np.array(vX), np.array(YX), [6., 150000.0, 5000.0])
  print a, x0, c_

  plt.plot(plt_rnge, [parabel(i,a,x0,c_) for i in plt_rnge])



flt = inter
fit_it()

flt = inter_2
fit_it()

flt = inter_3
fit_it()


plt.show()
