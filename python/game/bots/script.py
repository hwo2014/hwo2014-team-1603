from .base import Bot

class ScriptBot(Bot):
    def drive(self):
        command = self.script.next()
        if type(command) is str:
            self.switchLane(command)
        else:
            self.throttle(command)
