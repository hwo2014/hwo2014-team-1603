from game.bots.script import ScriptBot
import sys
import socket

def script(bot):
    while bot.myCar.position['piecePosition']['lap'] == 0:
        yield 0.6
    yield "Right"
    while True:
        yield 0.6
        
def tobiScript(bot):
    while True:
        yield 0.2



def main(host, port, name, key, mode):
  print("Connecting with parameters:")
  print("host={0}, port={1}, bot name={2}, key={3}".format(host, port, name, key))
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((host, int(port)))
  
  bot = ScriptBot(s, name, key)
  bot.script =tobiScript(bot)
  bot.socket = s
  bot.password = "Tobafrs213ega-6"
  if mode == 'create':
    bot.createRace()
  elif mode == 'join':
    bot.joinRace()
  elif mode == 'standalone':
    bot.join()
  else:
    print ("mode = create | join | standalone")
    
  bot.run()
  return bot
  

if __name__ == "__main__":
  if len(sys.argv) != 6:
      print("Usage: ./run host port botname botkey")
  else:
      host, port, name, key, mode = sys.argv[1:6]
  resultBot = main(host, port, name, key, mode)
  
  print [p['piecePosition']['inPieceDistance'] for p in resultBot.cars['red'].positions]
  print resultBot.cars['red'].speeds
  print resultBot.cars['red'].accelerations
  print resultBot.cars['red'].angles
  print resultBot.cars['red'].angleVelos
  print resultBot.cars['red'].angleAccs
  

  
