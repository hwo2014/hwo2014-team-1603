import socket
import sys

from game.bots.base import Bot
from script import ScriptBot

class TobiBot(Bot):
  def drive(self):
      self.throttle(0.68)

class LudwigBot(Bot):
  def drive(self):
      self.throttle(0.7)
      
class MatthiBot(Bot):
  def drive(self):
      self.throttle(0.55)


class Race(object):
  def __init__(self, bot):
    self.bot = bot
    
    

def main(host, port, name, key, mode):
  print("Connecting with parameters")
  print("host={0}, port={1}, bot name={2}, key={3}".format(host, port, name, key))
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((host, int(port)))
  
  if name == "Matthias":
    botClass = MatthiBot
  if name == "Tobi":
    botClass = TobiBot
  if name == "Ludwig":
    botClass = LudwigBot
  if name == "Script":
    botClass = ScriptBot
    
  bot = botClass(s, name, key)
  bot.socket = s
  bot.password = "Tobafrs213ega-6"
  if mode == 'create':
    bot.createRace()
  elif mode == 'join':
    bot.joinRace()
  elif mode == 'standalone':
    bot.join()
  else:
    print ("mode = create | join | standalone")
    
  bot.run()
  return bot
  

if __name__ == "__main__":
  if len(sys.argv) != 6:
      print("Usage: ./run host port botname botkey")
  else:
      host, port, name, key, mode = sys.argv[1:6]
  main(host, port, name, key, mode)
