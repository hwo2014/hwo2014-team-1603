//
//  ai.h
//  plusbot
//
//  Created by Matthias on 16.04.14.
//
//

#ifndef __plusbot__ai__
#define __plusbot__ai__

#include <iostream>
#include "car.h"
#include "track.h"


class AI {
public:
  Car me;
  Track track;
};

#endif /* defined(__plusbot__ai__) */
